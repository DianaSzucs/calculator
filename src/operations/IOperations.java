package operations;

public interface IOperations {
    /**
     * This method will be used to calculate a result
     * by a certain implementation
     */
    double calculateResult(double firstNumber, double secondNumber);

    /**
     * This method will be used to display a message
     * for a certain operation
     *
     * @return a message
     */
    String showPreResultMessage();
}

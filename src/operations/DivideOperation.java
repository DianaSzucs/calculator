package operations;

/**
 * This class is used to calculate the division of 2 given numbers
 * and display a message to keep track what operation is executed*/

public class DivideOperation implements IOperations {
    @Override
    public double calculateResult(double firstNumber, double secondNumber) {
        return firstNumber / secondNumber;
    }

    @Override
    public String showPreResultMessage() {
        return "The division of the numbers entered is:";
    }
}

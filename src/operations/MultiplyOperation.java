package operations;

/**
 * This class is used to calculate the multiplication of 2 given numbers
 * and display a message to keep track what operation is executed*/

public class MultiplyOperation implements IOperations{
    @Override
    public double calculateResult(double firstNumber, double secondNumber) {
        return firstNumber * secondNumber;
    }

    @Override
    public String showPreResultMessage() {
        return "The multiplication of the numbers entered is:";
    }
}

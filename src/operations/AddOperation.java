package operations;

/**
 * This class is used to calculate the sum of 2 given numbers
 * and display a message to keep track what operation is executed
 */

public class AddOperation implements IOperations {
    /**
     * This method will be used to add two doubles.
     *
     * @param firstNumber a value to be added
     * @param secondNumber the other value to be added
     * @return the sum of the two doubles
     */
    @Override
    public double calculateResult(double firstNumber, double secondNumber) {
        return firstNumber + secondNumber;
    }

    @Override
    public String showPreResultMessage() {
        return "The sum of the numbers entered is:";
    }
}

package operations;

/**
 * This class is used to calculate the raising to the power of 2 given numbers
 * and display a message to keep track what operation is executed
 */

public class SqrOperation implements IOperations {
    @Override
    public double calculateResult(double firstNumber, double secondNumber) {
        int pow = (int) secondNumber;
        double result = 1;
        if (pow == 0) return 1;
        else {
            for (int i = 1; i <= pow; i++) {
                result *= firstNumber;
            }
        }
        return result;
    }

    @Override
    public String showPreResultMessage() {
        return "The result of raising to the power is:";
    }
}

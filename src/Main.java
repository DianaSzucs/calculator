import operations.*;
import readdata.IReadFromConsole;
import readdata.ReadNumber;
import readdata.ReadOperation;
import readdata.ReadYesOrNo;

public class Main {

    public static void main(String[] args) {
        IOperations iOperation = null;
        IReadFromConsole iReadFromConsole;
        System.out.println("This is a console calculator. You can do the basic operations");
        while (true) {
            iReadFromConsole = new ReadOperation();
            String operation = iReadFromConsole.read("Please type in the math operation you would like to complete:\n" +
                    "+ for addition\n" +
                    "- for subtraction\n" +
                    "* for multiplication\n" +
                    "/ for division\n" +
                    "^ for raised to the n-th power");
            iReadFromConsole = new ReadNumber();
            double firstNumber = Double.parseDouble(iReadFromConsole.read("Enter the first number:"));
            double secondNumber = Double.parseDouble(iReadFromConsole.read("Enter the second number:"));
            switch (operation) {
                case "+":
                    iOperation = new AddOperation();
                    break;
                case "-":
                    iOperation = new SubtractOperation();
                    break;
                case "*":
                    iOperation = new MultiplyOperation();
                    break;
                case "/":
                    if (secondNumber == 0) {
                        System.out.println("cannot divide by 0");
                        break;
                    }
                    iOperation = new DivideOperation();
                    break;
                case "^":
                    iOperation = new SqrOperation();
                    break;
            }
            assert iOperation != null;
            System.out.println(iOperation.showPreResultMessage() + iOperation.calculateResult(firstNumber, secondNumber));
            iReadFromConsole = new ReadYesOrNo();
            String decision = iReadFromConsole.read("Do you want to calculate again?\n"+"Please type 'yes' or 'no' ");
            if (decision.toLowerCase().equals("no")) {
                return;
            }
        }
    }
}

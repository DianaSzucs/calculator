package readdata;
/**
 * This class is executed until we insert a valid double number
 */

import validate.ValidateDoubleNumber;

public class ReadNumber implements IReadFromConsole {
    @Override
    public String read(String message) {
        ValidateDoubleNumber validation = new ValidateDoubleNumber();
        ReadString readString = new ReadString();
        double result;
        while (true) {
            String number = readString.read(message);
            if (validation.validateInput(number)) {
                result = Double.parseDouble(number);
                break;
            }
        }
        return Double.toString(result);
    }
}

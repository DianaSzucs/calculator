package readdata;

import validate.ValidateOperation;

/**
 * This class is executed until we insert a valid operation
 */
public class ReadOperation implements IReadFromConsole {
    @Override
    public String read(String message) {
        ValidateOperation validation = new ValidateOperation();
        ReadString readString = new ReadString();
        String result;
        do {
            result = readString.read(message);
        } while (!validation.validateInput(result));
        return result;
    }
}

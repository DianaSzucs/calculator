package readdata;

import java.util.Scanner;

/**
 * This class is used display a message given as a parameter
 * and return the next line from console
 */
public class ReadString implements IReadFromConsole {
    @Override
    public String read(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        return scanner.nextLine();
    }
}

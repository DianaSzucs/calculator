package readdata;

public interface IReadFromConsole {
    /**
     * This method will be used return a string
     * which is read from console
     *
     * @param message a value to be displayed
     * @return a string which is read from console
     */

    String read(String message);
}

package validate;

public interface IValidation {
    /**
     * This method is used to validate the given string by a certain implementation
     */
    boolean validateInput(String input);
}

package validate;

/**
 * This class is used to check the yes/no choice
 */
public class ValidateYesOrNo implements IValidation {
    @Override
    public boolean validateInput(String input) {
        boolean check = false;
        String result = input.toLowerCase();
        if (input.equals("yes")) {
            check = true;
        } else if (input.equals("no")) {
            check = false;
        } else {
            System.out.println("Please choose between yes or no");
        }
        return check;
    }
}

package validate;

/**
 * This class is used to check if a string can be an operation
 * If true, it will be returned
 */
public class ValidateOperation implements IValidation {
    @Override
    public boolean validateInput(String input) {
        boolean validate = false;
        if (input.matches("[+/^*-]")) {
            validate = true;
        } else {
            System.out.println("Not a valid operation");
        }
        return validate;
    }
}

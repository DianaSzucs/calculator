package validate;

/**
 * This class is used to check if a given string is a double number
 */
public class ValidateDoubleNumber implements IValidation {

    @Override
    public boolean validateInput(String input) {
        boolean number = false;
        if (input.matches("\\s+-?\\d+(\\.\\d+)?") || input.matches("-?\\d+(\\.\\d+)?")) {
            number = true;
        } else {
            System.out.printf("\"%s\" is not a valid number.\n", input);
        }
        return number;
    }
}

